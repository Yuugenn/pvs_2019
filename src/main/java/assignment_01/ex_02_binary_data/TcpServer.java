package assignment_01.ex_02_binary_data;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by braunpet on 04.04.17.
 */
public class TcpServer
{
	public static void main( final String[] args ) throws Exception
	{
		final ServerSocket serverSocket = new ServerSocket( 6789 );

		while ( true ) {
			final Socket socket = serverSocket.accept();

			OutputStream outToClient = socket.getOutputStream();
			InputStream inFromClient = socket.getInputStream();

			byte[] a = new byte["Hallo".getBytes().length];

			inFromClient.read(a);

			String s = new String(a);



			System.out.println("Client: " + s);

			s = s.toUpperCase();

			a = s.getBytes();

			outToClient.write(a);
			outToClient.flush();
			outToClient.close();
			inFromClient.close();

//			final InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
//			final BufferedReader inFromClient = new BufferedReader(inputStreamReader);
//			final String input = inFromClient.readLine();

//					System.out.println("Received from Client: " + input);

//					final String[] inputs = input.split(",");
//
//					final String output = inputs[0].toUpperCase();
//
//					final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(socket.getOutputStream());
//					final BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
//					bufferedWriter.append(output).append('\n');
//					bufferedWriter.flush();
//					bufferedWriter.close();
//					inFromClient.close();
					socket.close();

		}
	}
}
