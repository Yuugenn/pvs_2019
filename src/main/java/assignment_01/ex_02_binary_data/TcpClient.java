package assignment_01.ex_02_binary_data;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by braunpet on 04.04.17.
 */
public class TcpClient
{
	public static void main( final String[] argv ) throws Exception
	{


		String output = new String(sendMessage("Hallo".getBytes()));

		System.out.println(output);

	}

	static byte[] sendMessage( final byte[] message) throws Exception{
		final Socket clientSocket = new Socket("localhost", 6789);

		OutputStream outToServer = clientSocket.getOutputStream();
		InputStream inFromServer = clientSocket.getInputStream();

		//byte[] source =

		outToServer.write(message);
		outToServer.flush();

		byte[] in = new byte[message.length];
		inFromServer.read(in);

		clientSocket.close();

		return in;
	}

//	private static String sendMessage( final int value, final String message ) throws Exception
//	{
//		final Socket clientSocket = new Socket( "localhost", 6789 );
//
//		final OutputStreamWriter outToServer = new OutputStreamWriter( clientSocket.getOutputStream( ) );
//		final InputStreamReader inputStreamReader = new InputStreamReader( clientSocket.getInputStream( ) );
//		final BufferedReader inFromServer = new BufferedReader( inputStreamReader );
//
//		final String input = message + "," + Integer.toString( value );
//
//		outToServer.append( input ).append( '\n' );
//		outToServer.flush( );
//
//		final String output = inFromServer.readLine( );
//		clientSocket.close( );
//
//		return output;
//	}

}
