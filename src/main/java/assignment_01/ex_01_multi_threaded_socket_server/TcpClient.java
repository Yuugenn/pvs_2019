package assignment_01.ex_01_multi_threaded_socket_server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * Created by braunpet on 04.04.17.
 */
public class TcpClient
{
	public static void main( final String[] argv ) throws Exception
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = br.readLine();
		int i = Integer.parseInt(br.readLine());

		System.out.println((sendMessage(i, s)));
	}

	private static String sendMessage( final int value, final String message ) throws Exception
	{
		final Socket clientSocket = new Socket( "localhost", 6790 );

		final OutputStreamWriter outToServer = new OutputStreamWriter( clientSocket.getOutputStream( ) );
		final InputStreamReader inputStreamReader = new InputStreamReader( clientSocket.getInputStream( ) );
		final BufferedReader inFromServer = new BufferedReader( inputStreamReader );

		final String input = message + "," + Integer.toString( value );

		outToServer.append( input ).append( '\n' );
		outToServer.flush( );

		final String output = inFromServer.readLine( );
		clientSocket.close( );

		return output;
	}
}
