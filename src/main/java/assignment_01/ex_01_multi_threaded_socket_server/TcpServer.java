package assignment_01.ex_01_multi_threaded_socket_server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by braunpet on 04.04.17.
 */
public class TcpServer
{
	public static void main( final String[] args ) throws Exception
	{
		final ServerSocket serverSocket = new ServerSocket( 6790 );

		while ( true ) {
			final Socket socket = serverSocket.accept();

			Thread thread = new Thread(() -> {
				try {

					final InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
					final BufferedReader inFromClient = new BufferedReader(inputStreamReader);
					final String input = inFromClient.readLine();

					System.out.println("Received from Client: " + input);

					final String[] inputs = input.split(",");

					final String output = inputs[0].toUpperCase();

					final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(socket.getOutputStream());
					final BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
					bufferedWriter.append(output).append('\n');
					bufferedWriter.flush();
					bufferedWriter.close();
					inFromClient.close();
					socket.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

			thread.start();
		}
	}
}
