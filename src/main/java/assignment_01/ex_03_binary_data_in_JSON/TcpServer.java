package assignment_01.ex_03_binary_data_in_JSON;

import com.owlike.genson.Genson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by braunpet on 04.04.17.
 */
public class TcpServer
{
	public static void main( final String[] args ) throws Exception
	{
		final ServerSocket serverSocket = new ServerSocket( 6789 );

		while ( true )
		{
			final Socket socket = serverSocket.accept( );
			final InputStreamReader inputStreamReader = new InputStreamReader( socket.getInputStream( ) );
			final BufferedReader inFromClient = new BufferedReader( inputStreamReader );
			final String input = inFromClient.readLine( );

			System.out.println( "Received from Client: " + input );

			Genson g = new Genson();
			Map<String, Object> map = new HashMap<>();

			map = g.deserialize(input, HashMap.class);

			String s = (String) map.get("data");


			byte[] a = s.toUpperCase().getBytes();

			System.out.println("NOT SER: " +  a);


			String json = g.serialize(a);


			System.out.println("SERIALIZED: " + json);
			final OutputStreamWriter outputStreamWriter = new OutputStreamWriter( socket.getOutputStream( ) );
			final BufferedWriter bufferedWriter = new BufferedWriter( outputStreamWriter );
			bufferedWriter.append( json ).append( '\n' );
			bufferedWriter.flush( );
			bufferedWriter.close( );
			inFromClient.close( );
			socket.close( );
		}
	}
}
