package assignment_01.ex_03_binary_data_in_JSON;

import com.owlike.genson.Genson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by braunpet on 04.04.17.
 */
public class TcpClient
{
	public static void main( final String[] argv ) throws Exception
	{
		sendMessage( 22, "James" );
	}

	public static void sendMessage( final int value, final String message ) throws Exception
	{
		final Socket clientSocket = new Socket( "localhost", 6789 );

		final OutputStreamWriter outToServer = new OutputStreamWriter( clientSocket.getOutputStream( ) );
		final InputStreamReader inputStreamReader = new InputStreamReader( clientSocket.getInputStream( ) );
		final BufferedReader inFromServer = new BufferedReader( inputStreamReader );

		final Map<String, Object> map = new HashMap( );
		map.put( "value", value );
		map.put( "message", message );

		//Genson g = new Genson();

		//byte[] a = "Test".getBytes();

		//String s = g.serialize(a);
		map.put("data", "hallo");


		final Genson genson = new Genson( );
		final String json = genson.serialize( map );

		System.out.println( json );

		outToServer.append( json ).append( '\n' );
		outToServer.flush( );

		final String output = inFromServer.readLine( );
		System.out.println(output);
		final Map<String, Object> inMap = genson.deserialize( output, HashMap.class );
		System.out.println( "RESPONSE FROM SERVER: value = " + inMap.get( "VALUE" ) );
		System.out.println( "RESPONSE FROM SERVER: message = " + inMap.get( "MESSAGE" ) );
		clientSocket.close( );
	}
}
