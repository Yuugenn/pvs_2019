package assignment_03.ex_01_java_servlet;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet( name = "Persons", urlPatterns = { "/person" } )
public class PersonServlet extends javax.servlet.http.HttpServlet
{

    private List<Person> personList = new ArrayList<>();

    @Override
    public void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws IOException
    {
        response.getWriter( ).println( "Personen: " );

        Map<String, String[]> parameterMap = request.getParameterMap();

        if(parameterMap.isEmpty()) {
            for (Person p : personList) {
                response.getWriter().println(p.toString());
            }
        }
        else {

            if(parameterMap.get("age") != null){
                for (Person p : personList) {
                    if (p.getAge() == Integer.parseInt(parameterMap.get("age")[0])){
                        response.getWriter().println(p.getName() + " " + p.getAge() + " " + p.isSingle());
                    }
                }
            }
        }

        response.setHeader("Content-Type", "application/json");
    }

    @Override
    public void doPost( final HttpServletRequest request, final HttpServletResponse response) throws IOException
    {
        String body = request.getReader().lines()
                .reduce("", (accumulator, actual) -> accumulator + actual);

        Genson g = new GensonBuilder().useConstructorWithArguments(true).create();
        final Person p = g.deserialize(body, Person.class);

        System.out.println(request.getHeader("User-Agent"));

        response.getWriter().print(p.getName() + " " + p.getAge() + " " + p.isSingle());

        personList.add(p);


    }

    //Stream example
    private List<Person> withLastName(String name){
        return this.personList.stream()
                .filter(p -> p.getName().equalsIgnoreCase(name))
                .collect(Collectors.toList());
    }

    //Test method
    public void addPerson(Person p){
        personList.add(p);
    }

    public boolean checkPerson(Person p){
        Person p2 = personList.get(2);

        return p.toString().equals(p2.toString());

    }
}
