package assignment_03.ex_01_java_servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet( name = "ServletExample", urlPatterns = { "/test" } )
public class DemoServlet extends javax.servlet.http.HttpServlet
{
	@Override
	protected void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws IOException
	{
		response.getWriter( ).print( "Wow!!!" );

	}
}
