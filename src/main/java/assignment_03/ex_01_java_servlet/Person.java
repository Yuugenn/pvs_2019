package assignment_03.ex_01_java_servlet;

public class Person {

    private String name;
    private int age;
    private boolean single;

    public Person(String name, int age, boolean single) {
        this.name = name;
        this.age = age;
        this.single = single;
    }

    public int getAge() {
        return age;
    }

    public boolean isSingle() {
        return single;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", single=" + single +
                '}';
    }
}
