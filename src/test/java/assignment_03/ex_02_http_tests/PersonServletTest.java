package assignment_03.ex_02_http_tests;

import assignment_03.ex_01_java_servlet.Person;
import assignment_03.ex_01_java_servlet.PersonServlet;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PersonServletTest {

    PersonServlet ps;
    HttpServletRequest request;
    HttpServletResponse response;
    StringWriter stringWriter;
    PrintWriter printWriter;
    Map<String, String[]> parameterMap;

    @Before
    public void setUp() throws IOException {
        ps = new PersonServlet();

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);

        parameterMap = new HashMap<>();

        ps.addPerson(new Person("Hans", 10, true));
        ps.addPerson(new Person("Franz", 12, false));

        when(request.getParameterMap()).thenReturn(parameterMap);

        stringWriter = new StringWriter();
        printWriter = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(printWriter);

        FileReader f = new FileReader("./src/test/resources/person.json");
        BufferedReader reader = new BufferedReader(f);

        when(request.getReader()).thenReturn(reader);
    }

    @Test
    public void testGetWithoutParameter() throws IOException {
        ps.doGet(request, response);

        printWriter.flush();

        assertTrue(stringWriter.toString().contains("Hans"));
        assertTrue(stringWriter.toString().contains("Franz"));

        //Reset the writer content
        stringWriter.getBuffer().setLength(0);
    }

    @Test
    public void testGetWithAgeParameter() throws IOException {
        //New Parameter to filter the persons
        parameterMap.put("age", new String[]{"10"});

        ps.doGet(request, response);
        printWriter.flush();

        assertTrue(stringWriter.toString().contains("Hans"));
        assertFalse(stringWriter.toString().contains("Franz"));
    }

    @Test
    public void testPost() throws IOException {

        ps.doPost(request, response);

        assertTrue(ps.checkPerson(new Person("bob", 10, true)));
    }
}
